drop database if exists uts_sister;
create database uts_sister;
use uts_sister;

create table tmtkuliah (
	  kode_mk varchar(6),
    nama_mk text,
    sks tinyint,
    primary key(kode_mk)
);

insert into tmtkuliah values('mk0120', 'mk_1', 3);
insert into tmtkuliah values('mk0121', 'mk_2', 3);
insert into tmtkuliah values('mk0122', 'mk_3', 2);
insert into tmtkuliah values('mk0123', 'mk_4', 3);
insert into tmtkuliah values('mk0124', 'mk_5', 3);
insert into tmtkuliah values('mk0125', 'mk_6', 3);
insert into tmtkuliah values('mk0126', 'mk_7', 3);
insert into tmtkuliah values('mk0127', 'mk_8', 2);
insert into tmtkuliah values('mk0128', 'mk_9', 3);
insert into tmtkuliah values('mk0129', 'mk_10', 2);
insert into tmtkuliah values('mk0130', 'mk_11', 3);

create table tjurusan (
	  kode_jur varchar(6),
    jurusan text,
    primary key(kode_jur)
);

insert into tjurusan values('jurtk1', 'teknik 1');
insert into tjurusan values('jurtk2', 'teknik 2');
insert into tjurusan values('jurtk3', 'teknik 3');
insert into tjurusan values('jurtk4', 'teknik 4');
insert into tjurusan values('jurtk5', 'teknik 5');
insert into tjurusan values('jurtk6', 'teknik 6');

create table tmahasiswa (
    nim varchar(14),
    nama text,
    kelamin text,
    tgl_masuk date,
    alamat text,
    kota text,
    telp text,
    primary key(nim)
);

insert into tmahasiswa values(
																		'41155050140001', 'mahasiswa 1', 'l', '2012-01-01',
																	'Jl.Pagarsih', 'Bandung', '085612345');
insert into tmahasiswa values('41155050140002', 'mahasiswa 2', 'l',
																	'2014-12-01', 'Jl. Jambe', 'Cirebon', '081123456789');
insert into tmahasiswa values('41155050140003', 'mahasiswa 3', 'p',
																	'2010-12-01', 'Jl. Satun', 'Semarang', '082123456789');
insert into tmahasiswa values('41155050140004', 'mahasiswa 4', 'l',
																	'2014-12-01', 'Jl. Ffsafsa', 'Cirebon', '081123456789');
insert into tmahasiswa values('41155050140005', 'mahasiswa 5', 'p',
																	'2010-12-01', 'Jl. Jambe', 'Cirebon', '081123456789');
insert into tmahasiswa values('41155050140006', 'mahasiswa 6', 'l',
																	'2004-12-01', 'Jl. Jambe', 'Cirebon', '081123456789');

create table tbiaya(
	  kode_biaya varchar(6),
    biaya_spp bigint,
    biaya_sks bigint,
    biaya_perpus bigint,
  	kode_jur varchar(6),
    primary key(kode_biaya)
);

insert into tbiaya values('kdbya1', 1500000, 20000, 60000, 'jurtk1');
insert into tbiaya values('kdbya2', 2500000, 20000, 60000, 'jurtk2');
insert into tbiaya values('kdbya3', 2000000, 20000, 60000, 'jurtk3');
insert into tbiaya values('kdbya4', 4000000, 20000, 60000, 'jurtk4');
insert into tbiaya values('kdbya5', 3000000, 20000, 60000, 'jurtk5');
insert into tbiaya values('kdbya6', 2000000, 20000, 60000, 'jurtk6');

create table trans_kul (
    tgl_trans date,
    semester tinyint,
    nim varchar(14),
    kode_mk varchar(6),
    kode_biaya varchar(6)
);

insert into trans_kul values('2016-01-01', 2, '41155050140001', 'mk0120', 'kdbya1');
insert into trans_kul values('2016-06-06', 2, '41155050140002', 'mk0122', 'kdbya2');
