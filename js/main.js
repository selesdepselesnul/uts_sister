var transKulTable = null;
function onDelete(tglTrans, kodeBiaya, nim) {
  bootbox.confirm(
    "Hapus transaksi ?",
    function(result) {
      if(result) {
        $.post(window.location.href  + "index.php/TransKul/delete/",
               {
                 tglTrans : tglTrans,
                 kodeBiaya: kodeBiaya,
                 nim: nim
               })
          .done(function(x) {
            var parsedX = jQuery.parseJSON(x);
            if(parsedX.success)
              toastr.success(parsedX.message);

              if(transKulTable != null)
                transKulTable.ajax.reload();
          });
      } else {

      }
    });
}

function onUpdate(tglTrans, kodeBiaya, nim) {
  $.post(window.location.href + 'index.php/TransKul/find/',
         {
           tglTrans: tglTrans,
           kodeBiaya: kodeBiaya,
           nim: nim
         })
    .done(function(x) {
      $('#nim').val(x.nim).trigger('change');
      $('#kode_mk').val(x.kode_mk).trigger('change');
      $('#kode_biaya').val(x.kode_biaya).trigger('change');
      $('#semester').val(x.semester).trigger('change');
      $('#nim').val(x.nim).trigger('change');
      $('#tglTrans').val(x.tgl_trans);
    });
}


$(document).ready(function() {

    $('#mahasiswaButton').click(function() {
      $.post(
        window.location.href + 'index.php/Mahasiswa/find/',
        { 'nim': $('#nim').select2('val') })
       .done(function(rawX) {
           var x = $.parseJSON(rawX);
           $('#nimMdl').val(x.nim);
           $('#namaMdl').val(x.nama);
           $('#kelaminMdl').val(x.kelamin);
           $('#tglTransMdl').val(x.tgl_masuk);
           $('#alamatMdl').val(x.alamat);
           $('#kotaMdl').val(x.kota);
           $('#telpMdl').val(x.telp);
           $('#mahasiswaModal').modal('show')
       });
    });

    $('#matakuliahButton').click(function() {
      $.post(
        window.location.href + 'index.php/Matakuliah/find/',
        { 'kodeMK': $('#matakuliah').select2('val') })
       .done(function(rawX) {
           var x = $.parseJSON(rawX);
           console.log(x);
           $('#kodeMKMdl').val(x.kode_mk);
           $('#namaMKMdl').val(x.nama_mk);
           $('#sksMdl').val(x.sks);
           $('#matakuliahModal').modal('show')
       });
    });

    $('#biayaButton').click(function() {
      $.post(
        window.location.href + 'index.php/Biaya/find/',
        { 'kodeBiaya': $('#biaya').select2('val') })
       .done(function(rawX) {
           var x = $.parseJSON(rawX);
           console.log(x);
           $('#kodeBiayaMdl').val(x.kode_biaya);
           $('#biayaSppMdl').val(x.biaya_spp);
           $('#biayaSksMdl').val(x.biaya_sks);
           $('#biayaPerpusMdl').val(x.biaya_perpus);
           $('#kodeJurusanMdl').val(x.kode_jur);
           $('#biayaModal').modal('show')
       });
    });


    $('.select2').select2({
        dropdownAutoWidth: true
    });

    $('#tglTrans').datepicker({
      format: 'yyyy-mm-dd'
    });

    transKulTable = $('#transKul').DataTable( {
        "ajax": window.location.href  + "index.php/TransKul/get/",
        "columns": [
            { "data": "tgl_trans" },
            { "data": "nim" },
            { "data": "nama" },
            { "data": "nama_mk" },
            { "data": "kode_biaya" },
            { "data": "biaya_spp" },
            { "data": "biaya_sks" },
            { "data": "biaya_perpus" },
            { "data": "jurusan" },
            { "data": "semester" },
            {
              "data": null,
              "render": function(x) {
                return '<button class="btn btn-default"'+
                ' onclick="onDelete(\''+x.tgl_trans
                                + '\', \''
                                + x.kode_biaya
                                + '\',\'' +x.nim+ '\')"><span class="glyphicon glyphicon-trash"></span>delete</button>'
              }
            },
            {
              "data": null,
              "render": function(x) {
                return '<button class="btn btn-default"'+
                ' onclick="onUpdate(\''+x.tgl_trans
                                  +'\', \''
                                  + x.kode_biaya+'\',\''
                                  + x.nim + '\')"><span class="glyphicon glyphicon-edit"></span>update</button>'
              }
            }
        ],
        "deferRender": true,

    });

    function initSelect2(route, id) {
      $.get(window.location.href + 'index.php/'+route)
       .done(function(xs) {
            $("#"+id).select2({
              data: jQuery.parseJSON(xs)
            });
       });
    }

    initSelect2('Mahasiswa/get_nim', 'nim');
    initSelect2('Matakuliah/get_kodemk', 'matakuliah');
    initSelect2('Biaya/get_kodebiaya', 'biaya');

    var semester = _.map(_.range(1, 9),
                        function(x) {
                          return { id: x, text: x }
                        });

     $('#semester').select2({
       data: semester
     });



     $("#storeTransKul").on( "submit", function( event ) {
       event.preventDefault();

       bootbox.confirm(
         "Simpan transaksi",
         function(result){
           if(result) {
              console.log('disimpan !');

              $.post(
                window.location.href + 'index.php/TransKul/store/',
                {
                  tglTrans: $("#tglTrans").val(),
                  nim : $("#nim").select2("val"),
                  matakuliah: $("#matakuliah").select2("val"),
                  biaya: $("#biaya").select2("val"),
                  semester: $("#semester").select2("val")
                }).done(function(x) {
                  var parsedX = jQuery.parseJSON(x);
                  console.log(parsedX);
                  if(parsedX.success)
                    toastr.success(parsedX.message);
                  transKulTable.ajax.reload();

                });
              console.log(nim);
            } else {
              console.log('tidak disimpan !');
            }

          }
       );
     });
});
