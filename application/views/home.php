<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trans Kul | UTS Sister</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="DataTables/datatables.min.css" rel="stylesheet"/>
    <link href="css/select2.min.css" rel="stylesheet" />
    <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <link href="css/toastr.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/underscore-min.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/toastr.min.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <h2>Trans Kul</h2>
      </div>
      <hr>
      <div class="row">
        <table id="transKul" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>tgl trans</th>
                        <th>nim</th>
                        <th>nama</th>
                        <th>nama mk</th>
                        <th>kode biaya</th>
                        <th>biaya spp</th>
                        <th>biaya sks</th>
                        <th>biaya perpus</th>
                        <th>jurusan</th>
                        <th>semester</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
        </table>

      </div>
      <hr>
      <div class="row">
        <form id="storeTransKul" class="form-horizontal">
          <div class="form-group">
            <label class="control-label col-md-2" for="email">Tgl Trans</label>
            <div class="col-md-10">
              <input type="date" id="tglTrans" class="datepicker">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2" for="email">Mahasiswa</label>
            <div class="col-md-10">
              <select id="nim" style="width: 28%">

              </select>
              <button class="btn btn-default" id="mahasiswaButton" type="button">
                <span class="glyphicon glyphicon-new-window"></span>detail</button>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Matakuliah</label>
            <div class="col-md-10">
              <select id="matakuliah" style="width: 18%">

              </select>
              <button class="btn btn-default" id="matakuliahButton" type="button">
                <span class="glyphicon glyphicon-new-window"></span>detail</button>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Kode biaya</label>
            <div class="col-md-10">
              <select id="biaya" style="width: 10%">

              </select>
              <button class="btn btn-default" id="biayaButton" type="button">
                <span class="glyphicon glyphicon-new-window"></span>detail</button>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Semester</label>
            <div class="col-md-10">
              <select id="semester" style="width:6%">

              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
              <button type="submit" id="saveTransButton" class="btn btn-default">
                <span class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
            </div>
          </div>
        </form>
      </div>

    </div>
    <div id="mahasiswaModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Mahasiswa</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Nim</label>
              <div class="col-md-10">
                <input type="text" id="nimMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">kelamin</label>
              <div class="col-md-10">
                <input type="text" id="kelaminMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Nama</label>
              <div class="col-md-10">
                <input type="text" id="namaMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Tgl masuk</label>
              <div class="col-md-10">
                <input type="text" id="tglTransMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Alamat</label>
              <div class="col-md-10">
                <input type="text" id="alamatMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Kota</label>
              <div class="col-md-10">
                <input type="text" id="kotaMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Telp</label>
              <div class="col-md-10">
                <input type="text" id="telpMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div id="matakuliahModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Matakuliah</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Kode MK</label>
              <div class="col-md-10">
                <input type="text" id="kodeMKMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Nama MK</label>
              <div class="col-md-10">
                <input type="text" id="namaMKMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">SKS</label>
              <div class="col-md-10">
                <input type="text" id="sksMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div id="biayaModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Biaya</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Kode Biaya</label>
              <div class="col-md-10">
                <input type="text" id="kodeBiayaMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Biaya SPP</label>
              <div class="col-md-10">
                <input type="text" id="biayaSppMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Biaya SKS</label>
              <div class="col-md-10">
                <input type="text" id="biayaSksMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Biaya Perpus</label>
              <div class="col-md-10">
                <input type="text" id="biayaPerpusMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-md-2" for="email">Kode Jurusan</label>
              <div class="col-md-10">
                <input type="text" id="kodeJurusanMdl" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script src="js/main.js"></script>
  </body>
</html>
