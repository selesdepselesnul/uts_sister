<?php
 class Mahasiswa extends CI_Controller {

   public function __construct()
   {
       parent::__construct();
       $this->load->database();
       $this->load->helper('url');
   }


   public function find()
   {
       $nim = $this->input->post('nim');
       $result = $this->db
                      ->get_where(
                             'tmahasiswa',
                             [
                               'nim' => $nim
                             ])
                      ->result();
       echo json_encode((object)$result[0]);
   }

   public function get_nim()
   {
       $xs = $this->db->get('tmahasiswa')->result();
       $data = array_map(function($x) {
           return (object)[
               'id' => $x->nim,
               'text' => $x->nim . ' | ' . $x->nama
             ];
       }, $xs);
       echo json_encode($data);
   }
 }
