<?php
class Home extends CI_Controller {

        public function __construct()
        {
          parent::__construct();
          $this->load->database();
          $this->load->helper('url');
        }

        public function index()
        {
          $this->load->view('home');;
        }

        public function find_trans_kul()
        {
          $tgl_trans = $this->input->post('tglTrans');
          $kode_biaya = $this->input->post('kodeBiaya');
          $nim = $this->input->post('nim');

          $result = $this->db
                         ->get_where(
                                'trans_kul',
                                [
                                  'tgl_trans' => $tgl_trans,
                                  'kode_biaya' => $kode_biaya,
                                  'nim' => $nim
                                ])
                         ->result();
           header('Content-Type: application/json');
           echo json_encode((object)$result[0]);
        }

        public function find_mhs()
        {
            $nim = $this->input->post('nim');
            $result = $this->db
                           ->get_where(
                                  'tmahasiswa',
                                  [
                                    'nim' => $nim
                                  ])
                           ->result();
            echo json_encode((object)$result[0]);
        }

        public function find_mk()
        {
            $kode_mk = $this->input->post('kodeMK');
            $result = $this->db
                           ->get_where(
                                  'tmtkuliah',
                                  [
                                    'kode_mk' => $kode_mk
                                  ])
                           ->result();
            echo json_encode((object)$result[0]);
        }

        public function find_biaya()
        {
            $kode_biaya = $this->input->post('kodeBiaya');
            $result = $this->db
                           ->get_where(
                                  'tbiaya',
                                  [
                                    'kode_biaya' => $kode_biaya
                                  ])
                           ->result();
            echo json_encode((object)$result[0]);
        }


        private function proc_trans_kul($func)
        {
            $raw_trans_kuls = $this->db->get('trans_kul')->result();
            $trans_kul = array_map(function($x) use($func) {

            $get_where = function($table_name, $id_name, $val) {
              return $this->db
                   ->get_where(
                          $table_name,
                          [$id_name => $val])
                   ->result()[0];
            };

            $mhs = $get_where('tmahasiswa', 'nim', $x->nim);
            $mt_kuliah = $get_where('tmtkuliah', 'kode_mk', $x->kode_mk);
            $biaya = $get_where('tbiaya', 'kode_biaya', $x->kode_biaya);
            $jurusan = $get_where('tjurusan', 'kode_jur', $biaya->kode_jur);

            return (object)$func($x, $mhs, $mt_kuliah, $biaya, $jurusan);
          }, $raw_trans_kuls);
          $data = (object)[
            'data' => $trans_kul
          ];
          header('Content-Type: application/json');
          echo json_encode($data);
        }

        public function get_trans_kul()
        {
          $this->proc_trans_kul(function($trans_kul,
                                  $mhs,
                                  $mt_kuliah,
                                  $biaya,
                                  $jurusan){
            return [
              'tgl_trans' => $trans_kul->tgl_trans,
              'kode_biaya' => $trans_kul->kode_biaya,
              'nim' => $trans_kul->nim,
              'nama' => $mhs->nama,
              'nama_mk' => $mt_kuliah->nama_mk,
              'biaya_spp' => $biaya->biaya_spp,
              'biaya_sks' => $biaya->biaya_sks,
              'biaya_perpus' => $biaya->biaya_perpus,
              'jurusan' => $jurusan->jurusan,
              'semester' => $trans_kul->semester
            ];
          });
        }

        public function get_options()
        {
            $this->proc_trans_kul(function($trans_kul,
                                    $mhs,
                                    $mt_kuliah,
                                    $biaya,
                                    $jurusan){
              return [
                'nim' => $trans_kul->nim,
                'nama' => $mhs->nama,
                'kode_mk' => $mt_kuliah->kode_mk,
                'nama_mk' => $mt_kuliah->nama_mk,
                'kode_jur' => $jurusan->kode_jur,
                'kode_biaya' => $biaya->kode_biaya
              ];
            });
        }

        public function get_nim()
        {
            $xs = $this->db->get('tmahasiswa')->result();
            $data = array_map(function($x) {
                return (object)[
                    'id' => $x->nim,
                    'text' => $x->nim . ' | ' . $x->nama
                  ];
            }, $xs);
            echo json_encode($data);
        }

        public function get_biaya()
        {
            $xs = $this->db->get('tbiaya')->result();
            $data = array_map(function($x) {
                return (object)[
                    'id' => $x->kode_biaya,
                    'text' => $x->kode_biaya
                  ];
            }, $xs);
            echo json_encode($data);
        }

        public function get_mk()
        {
            $xs = $this->db->get('tmtkuliah')->result();
            $data = array_map(function($x) {
                return (object)[
                    'id' => $x->kode_mk,
                    'text' => $x->kode_mk . ' | ' . $x->nama_mk
                  ];
            }, $xs);
            echo json_encode($data);
        }

        public function delete_trans_kul()
        {
          $kode_biaya = $this->input->post('kodeBiaya');
          $tgl_trans = $this->input->post('tglTrans');
          $nim = $this->input->post('nim');

          $this->db->delete('trans_kul',[
            'tgl_trans' => $tgl_trans,
            'kode_biaya' => $kode_biaya,
            'nim' => $nim
          ]);
          echo json_encode((object)['success' => true,
                            'message' => 'berasil di hapus!']);
        }

        public function store_transkul()
        {
            $nim = $this->input->post('nim');
            $kode_mk = $this->input->post('matakuliah');
            $kode_biaya = $this->input->post('biaya');
            $semester = $this->input->post('semester');
            $tgl_trans = $this->input->post('tglTrans');

            $result = $this->db
                 ->get_where(
                        'trans_kul',
                        [
                          'tgl_trans' => $tgl_trans,
                          'kode_biaya' => $kode_biaya,
                          'nim' => $nim
                        ])
                 ->result();

             $data = [
                   'tgl_trans' => $tgl_trans,
                   'nim' => $nim,
                   'kode_mk' => $kode_mk,
                   'kode_biaya' => $kode_biaya,
                   'semester' => $semester
                 ];
            if(count($result) == 0) {
                $this->db->insert('trans_kul', $data);
                echo json_encode((object)
                                  ['success' => true,
                                  'message' => 'Berhasil di simpan !']);
            } else {
                $this->db->update('trans_kul',
                                  $data,
                                  [
                                    'tgl_trans' => $tgl_trans,
                                    'kode_biaya' => $kode_biaya
                                  ]);
                echo json_encode((object)['success' => true,
                                'message' => 'Berhasil di perbaharui !']);
            }


        }
}
