<?php
 class Biaya extends CI_Controller {

   public function __construct()
   {
     parent::__construct();
     $this->load->database();
     $this->load->helper('url');
   }


   public function get_kodebiaya()
   {
       $xs = $this->db->get('tbiaya')->result();
       $data = array_map(function($x) {
           return (object)[
               'id' => $x->kode_biaya,
               'text' => $x->kode_biaya
             ];
       }, $xs);
       echo json_encode($data);
   }

   public function find()
   {
       $kode_biaya = $this->input->post('kodeBiaya');
       $result = $this->db
                      ->get_where(
                             'tbiaya',
                             [
                               'kode_biaya' => $kode_biaya
                             ])
                      ->result();
       echo json_encode((object)$result[0]);
   }
 }
