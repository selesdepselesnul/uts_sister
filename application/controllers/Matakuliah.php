<?php
class Matakuliah extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->helper('url');
  }

  public function find()
  {
      $kode_mk = $this->input->post('kodeMK');
      $result = $this->db
                     ->get_where(
                            'tmtkuliah',
                            [
                              'kode_mk' => $kode_mk
                            ])
                     ->result();
      echo json_encode((object)$result[0]);
  }

  public function get_kodemk()
  {
      $xs = $this->db->get('tmtkuliah')->result();
      $data = array_map(function($x) {
          return (object)[
              'id' => $x->kode_mk,
              'text' => $x->kode_mk . ' | ' . $x->nama_mk
            ];
      }, $xs);
      echo json_encode($data);
  }
}
